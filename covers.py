import os
import platform
import tempfile
import subprocess

#pip install pdf2image unidecode
if platform.system().lower().startswith('linux'):
    subprocess.check_output('sudo apt install poppler-utils'.split())
    subprocess.check_output('pip install unidecode pdf2image'.split())

if platform.system().lower().startswith('win'):
    subprocess.check_output('pip install unidecode pdf2image'.split(),shell=True)

from pdf2image import convert_from_path
from unidecode import unidecode

def remove_accents(accented_string):
    unaccented_string = unidecode(accented_string)
    return unaccented_string

#mkdir covers folder
os.makedirs('covers', exist_ok=True)

if platform.system().lower().startswith('win'):
    def pdf_to_png(pdf_name):
        with tempfile.TemporaryDirectory() as path:
                images_from_path = convert_from_path(pdf_path=f'books/{pdf_name}',
                output_folder='covers',
                fmt="png",
                size=(337, None),
                output_file=remove_accents(pdf_name[:-4]),
                single_file=True,
                poppler_path=poppler_path)

    #unzip poppler
    import zipfile
    with zipfile.ZipFile('scripts/poppler.zip', 'r') as zip_ref:
        zip_ref.extractall('scripts')
    poppler_path = r'' + os.getcwd()+'/scripts/poppler-20.11.0/bin'

    #make png 
    for filename in os.listdir('books'):
        if filename.endswith(".pdf"):
            pdf_to_png(filename)
    
    #remove popple folder
    import shutil
    shutil.rmtree('scripts/poppler-20.11.0')

if platform.system().lower().startswith('linux'):
    def pdf_to_png(pdf_name):
        with tempfile.TemporaryDirectory() as path:
                images_from_path = convert_from_path(pdf_path=f'books/{pdf_name}',
                output_folder='covers',
                fmt="png",
                size=(337, None),
                output_file=remove_accents(pdf_name[:-4]),
                single_file=True)
                
    #make png 
    for filename in os.listdir('books'):
        if filename.endswith(".pdf"):
            pdf_to_png(filename)
    
output_msg='''
#######################################
####### Successfully completed! #######
#######################################
'''
print(output_msg)